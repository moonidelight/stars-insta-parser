package amoapi

import (
	"encoding/json"
	"fmt"
	"gitlab.com/devcrimson/stars-rating-parser/oauth"
	"gitlab.com/devcrimson/stars-rating-parser/utils"
	"io"
	"net/http"
	"os"
	"strings"
	"time"
)

type API struct {
	referer, refresh string
	tokenLastUpdated time.Time
	client           *http.Client
}

func (a *API) checkExpired() error {
	if a.tokenLastUpdated.Add(time.Minute * 20).Before(time.Now()) {
		const template = `{"client_id":"%s","client_secret":"%s","grant_type":"refresh_token","refresh_token":"%s","redirect_uri":"%s"}`
		resp, err := http.Post(
			fmt.Sprintf("https://%s/oauth2/access_token", a.referer),
			"application/json",
			strings.NewReader(fmt.Sprintf(
				template,
				os.Getenv("OAUTH_CLIENT_ID"),
				os.Getenv("OAUTH_CLIENT_SECRET"),
				a.refresh,
				os.Getenv("OAUTH_REDIRECT_URI"),
			)),
		)
		if err != nil {
			return err
		}
		bytes, err := io.ReadAll(resp.Body)
		if err != nil {
			return err
		}
		tokenResp := new(oauth.TokensResult)
		err = json.Unmarshal(bytes, tokenResp)
		if err != nil {
			return err
		}
		a.refresh = tokenResp.RefreshToken
		a.client = createClientWithAuth(tokenResp.AccessToken)
		return nil
	}
	return nil
}

type EmbeddedAdditionalFieldsResponse struct {
	Embedded struct {
		Fields []FieldsField `json:"custom_fields"`
	} `json:"_embedded"`
}

type FieldsField struct {
	Name string `json:"name"`
	Code string `json:"code"`
	Type string `json:"type"`
}

func (a *API) GetAdditionalFields() ([]FieldsField, error) {
	err := a.checkExpired()
	if err != nil {
		return nil, err
	}
	resp, err := a.client.Get(fmt.Sprintf("https://%s/api/v4/contacts/custom_fields", a.referer))
	if err != nil {
		return nil, err
	}
	body, err := utils.ConsumeResponse(resp)
	embeddedResponse := new(EmbeddedAdditionalFieldsResponse)
	return embeddedResponse.Embedded.Fields, json.Unmarshal(body, embeddedResponse)
}

func (a *API) CreateAdditionalFields() error {
	err := a.checkExpired()
	if err != nil {
		return err
	}
	const template = `{"name":"%s","code":"%s","type":"text"}`
	var fieldsJSON []string
	for _, n := range []string{"Name", "Username", "Country", "Topics", "Followers", "Engagement", "Alive"} {
		fieldsJSON = append(fieldsJSON, fmt.Sprintf(template, n, strings.ToUpper(n)))
	}
	resp, err := a.client.Post(
		fmt.Sprintf("https://%s/api/v4/contacts/custom_fields", a.referer),
		"application/json",
		strings.NewReader("["+strings.Join(fieldsJSON, ",")+"]"),
	)
	if err != nil {
		return err
	}
	_, err = utils.ConsumeResponse(resp)
	return err
}

type CustomFieldResponse struct {
	Name   string `json:"field_name"`
	Code   string `json:"field_code"`
	Values []struct {
		Value string `json:"value"`
	} `json:"values"`
}

func (c *CustomFieldResponse) Value() string {
	return c.Values[0].Value
}

type embeddedContactsFullResponse struct {
	Embedded struct {
		Contacts []*ContactsContact `json:"contacts"`
	} `json:"_embedded"`
}

type ContactsContact struct {
	ID     int                   `json:"id"`
	Name   string                `json:"name"`
	Fields ContactsContactFields `json:"custom_fields_values"`
}

type ContactsContactFields []CustomFieldResponse

func (c ContactsContactFields) Get(name string) string {
	for _, v := range c {
		if v.Name == name {
			return v.Value()
		}
	}
	return ""
}

func (a *API) GetContacts() ([]*ContactsContact, error) {
	err := a.checkExpired()
	if err != nil {
		return nil, err
	}
	resp, err := a.client.Get(fmt.Sprintf("https://%s/api/v4/contacts", a.referer))
	if err != nil {
		return nil, err
	}
	bytes, err := utils.ConsumeResponse(resp)
	if err != nil {
		return nil, err
	}
	jsonResp := new(embeddedContactsFullResponse)
	return jsonResp.Embedded.Contacts, json.Unmarshal(bytes, jsonResp)
}

type embeddedContactsWithRIDResponse struct {
	Embedded struct {
		Contacts []struct {
			ID        int    `json:"id"`
			RequestID string `json:"request_id"`
		} `json:"contacts"`
	} `json:"_embedded"`
}

type ContactToAdd struct {
	Name   string
	Fields []*CustomField
}

func (c *ContactToAdd) ToJSON() string {
	const template = `{"name":"%s","request_id":"%s","custom_fields_values":%s}`
	return fmt.Sprintf(
		template,
		c.Name,
		c.Name,
		utils.SerializeArray(c.Fields),
	)
}

type AddedContact struct {
	ID   int    `db:"contact_id"`
	Name string `db:"name"`
}

func (a *AddedContact) String() string {
	return fmt.Sprintf(
		`AddedContact(id=%d, name="%s")`,
		a.ID, a.Name,
	)
}

func (a *API) AddContacts(contacts []*ContactToAdd) ([]*AddedContact, error) {
	err := a.checkExpired()
	if err != nil {
		return nil, err
	}
	resp, err := a.client.Post(
		fmt.Sprintf("https://%s/api/v4/contacts", a.referer),
		"application/json",
		strings.NewReader(utils.SerializeArray(contacts)),
	)
	if err != nil {
		return nil, err
	}
	body, err := utils.ConsumeResponse(resp)
	if err != nil {
		return nil, err
	}
	jsonBody := new(embeddedContactsWithRIDResponse)
	err = json.Unmarshal(body, jsonBody)
	if err != nil {
		return nil, err
	}
	var result []*AddedContact
	for _, contact := range jsonBody.Embedded.Contacts {
		result = append(result, &AddedContact{
			ID:   contact.ID,
			Name: contact.RequestID,
		})
	}
	return result, nil
}

type ContactToUpdate struct {
	ID     int
	Fields []*CustomField
}

func (c *ContactToUpdate) ToJSON() string {
	const template = `{"id":%d,"custom_fields_values":%s}`
	return fmt.Sprintf(
		template,
		c.ID,
		utils.SerializeArray(c.Fields),
	)
}

func (a *API) UpdateContacts(contacts []*ContactToUpdate) error {
	err := a.checkExpired()
	if err != nil {
		return err
	}
	req, err := http.NewRequest(
		"PATCH",
		fmt.Sprintf("https://%s/api/v4/contacts", a.referer),
		strings.NewReader(utils.SerializeArray(contacts)),
	)
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := a.client.Do(req)
	if err != nil {
		return err
	}
	_, err = utils.ConsumeResponse(resp)
	return err
}

func NewAPI(referer string, tokens *oauth.TokensResult) *API {
	return &API{
		referer,
		tokens.RefreshToken,
		time.Now(),
		createClientWithAuth(tokens.AccessToken),
	}
}

func createClientWithAuth(accessToken string) *http.Client {
	return &http.Client{
		Transport: utils.NewAuthTransport(
			fmt.Sprint("Bearer ", accessToken),
			http.DefaultTransport,
		),
	}
}
