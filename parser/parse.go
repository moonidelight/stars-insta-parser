package parser

import (
	"errors"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"io"
	"net/http"
	"strings"
)

var ParseError = errors.New("cannot parse document")

func Parse(reader io.Reader) (_ []*Entry, reterr error) {
	defer func() {
		if r := recover(); r != nil {
			reterr = ParseError
		}
	}()
	doc, err := goquery.NewDocumentFromReader(reader)
	if err != nil {
		return nil, err
	}
	var entries []*Entry
	doc.Find("table.table tbody tr").Map(func(_ int, selection *goquery.Selection) string {
		tds := selection.Find("td.align-middle")
		if tds.Size() == 0 {
			return ""
		}
		if err != nil {
			panic(err)
		}
		entry := &Entry{
			Name:       processInput(tds.Get(2).FirstChild.Data),
			Username:   processInput(tds.Eq(2).Find("a").Text()),
			Country:    processInput(tds.Eq(3).Find("a").Text()),
			Followers:  processInput(tds.Eq(5).Text()),
			Engagement: processInput(tds.Eq(6).Text()),
			Topics:     "",
		}
		var topics []string
		tds.Eq(4).Find("a").Each(func(i int, selection *goquery.Selection) {
			topics = append(topics, selection.Text())
		})
		entry.Topics = strings.Join(topics, ";")
		entries = append(entries, entry)
		return ""
	})
	return entries, nil
}

func processInput(s string) string {
	return strings.ReplaceAll(
		strings.Trim(s, " ⠀\n\r\t"),
		"\n",
		" ",
	)
}

const SiteUrl = "https://starngage.com/app/global/influencer/ranking/%s"

func ParseFromSite(country string) ([]*Entry, error) {
	resp, err := http.Get(fmt.Sprintf(SiteUrl, country))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	return Parse(resp.Body)
}
