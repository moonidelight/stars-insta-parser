package main

import (
	"encoding/json"
	"fmt"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"gitlab.com/devcrimson/stars-rating-parser/amoapi"
	db2 "gitlab.com/devcrimson/stars-rating-parser/db"
	"gitlab.com/devcrimson/stars-rating-parser/oauth"
	"gitlab.com/devcrimson/stars-rating-parser/parser"
	"io/ioutil"
	"os"
	"time"
)

func main() {
	err := godotenv.Load("staging.env")
	dur, err := time.ParseDuration(os.Getenv("REFRESH_DURATION"))
	perr(err)
	entries, err := parser.ParseFromSite(os.Getenv("PARSER_COUNTRY"))
	perr(err)
	//auth, err := oauth.RequestForAuth()
	auth := &oauth.AuthResult{
		Referer: fmt.Sprint(os.Getenv("SUBDOMAIN"), ".amocrm.ru"),
		Code:    os.Getenv("oauth_code"),
	}
	file := "token.json"
	var tokens *oauth.TokensResult
	if tokenFile(file) {
		tokens = getToken(file)
	} else {
		tokens, err = oauth.RetrieveTokens(auth)
		perr(err)
		writeToken(file, tokens)
	}
	fmt.Println(tokens.RefreshToken)

	db := db2.Connect()
	reports := db.Get()
	var newEntries []*parser.Entry
	for _, v := range entries {
		if !existsInEntries(v.Username, reports) {
			newEntries = append(newEntries, v)
		}
	}
	api := amoapi.NewAPI(auth.Referer, tokens)
	_ = api.CreateAdditionalFields()
	if len(newEntries) != 0 {
		db.Create(newEntries)
		_, err = api.AddContacts(toContactSlice(newEntries))
		perr(err)
	}
	for {
		time.Sleep(dur)
		entries, err = parser.ParseFromSite(os.Getenv("PARSER_COUNTRY"))
		perr(err)
		contacts, err := api.GetContacts()
		perr(err)
		reports = db.Get()
		var newContacts []*amoapi.ContactToAdd
		var updatedContacts []*amoapi.ContactToUpdate
		var deleted []*parser.Entry
		newEntries = make([]*parser.Entry, 0)
		for _, v := range entries {
			check := false
			for _, e := range reports {
				if v.Username == e.Username {
					check = true
					break
				}
			}
			if !check {
				newEntries = append(newEntries, v)
			}
		}
		for _, v := range reports {
			check := false
			for _, e := range entries {
				if v.Username == e.Username {
					check = true
					break
				}
			}
			if !check {
				deleted = append(deleted, v)
			}
		}
		for _, v := range toContactSlice(newEntries) {
			if contact := getContactWithName(v.Name, contacts); contact != nil {
				updatedContacts = append(updatedContacts, &amoapi.ContactToUpdate{
					ID:     contact.ID,
					Fields: v.Fields,
				})
			} else {
				newContacts = append(newContacts, &amoapi.ContactToAdd{
					Name:   v.Name,
					Fields: v.Fields,
				})
			}
		}
		var deletedContacts []*amoapi.ContactToUpdate
		for _, v := range contacts {
			if existsInEntries(v.Name, deleted) {
				deletedContacts = append(deletedContacts, &amoapi.ContactToUpdate{
					ID: v.ID,
					Fields: []*amoapi.CustomField{{
						Name:  "alive",
						Value: "0",
					}},
				})
			}
		}
		if len(deleted) != 0 {
			db.Update(deleted)
		}
		if len(newEntries) != 0 {
			db.Create(newEntries)
		}
		_, err = api.AddContacts(newContacts)
		if len(newContacts) != 0 {
			perr(err)
		}
		if len(deletedContacts) != 0 {
			perr(api.UpdateContacts(append(updatedContacts, deletedContacts...)))
		}
	}
}

type toContact interface {
	ToContact() *amoapi.ContactToAdd
}

func toContactSlice[T toContact](entries []T) []*amoapi.ContactToAdd {
	r := make([]*amoapi.ContactToAdd, 0, len(entries))
	for _, v := range entries {
		r = append(r, v.ToContact())
	}
	return r
}

func getContactWithName(name string, contacts []*amoapi.ContactsContact) *amoapi.ContactsContact {
	for _, v := range contacts {
		if v.Name == name {
			return v
		}
	}
	return nil
}

func existsInEntries(name string, entries []*parser.Entry) bool {
	for _, v := range entries {
		if v.Username == name {
			return true
		}
	}
	return false
}

func perr(err error) {
	if err != nil {
		panic(err)
	}
}
func tokenFile(file string) bool {
	if _, err := os.Stat(file); err == nil {
		return true
	}
	_, err := os.Create(file)
	if err != nil {
		perr(err)
	}
	return false
}

func writeToken(file string, token *oauth.TokensResult) {
	c, err := json.Marshal(token)
	perr(err)
	err = ioutil.WriteFile(file, c, 0644)
	perr(err)
}

func getToken(file string) *oauth.TokensResult {
	c, err := ioutil.ReadFile(file)
	perr(err)
	var token *oauth.TokensResult
	err = json.Unmarshal(c, &token)
	perr(err)
	return token
}
