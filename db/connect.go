package db

import (
	"gitlab.com/devcrimson/stars-rating-parser/parser"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"os"
)

type Repo struct {
	DB *gorm.DB
}

func Connect() *Repo {
	dsn := os.Getenv("db_connection_string")
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	err = db.AutoMigrate(&parser.Entry{})
	if err != nil {
		panic(err)
	}
	return &Repo{
		db,
	}
}

func (repo *Repo) Create(entries []*parser.Entry) {
	repo.DB.Create(&entries)
}

func (repo *Repo) Get() []*parser.Entry {
	var entries []*parser.Entry
	//repo.DB.Limit(100).Find(&entries)
	repo.DB.Find(&entries)
	return entries
}

func (repo *Repo) Update(entries []*parser.Entry) {
	for _, v := range entries {
		repo.DB.Model(&parser.Entry{}).Where("username = ?", v.Username).Update("alive", "0")
	}
}
